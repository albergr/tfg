
import os
import sys
import subprocess
import fileinput

import numpy as np
import src.data.ml_100k as generator100
import src.data.ml_20m as generator20


genres = ['(no genres listed)', 'Action', 'Adventure', 'Animation', 'Children', 'Comedy',
            'Crime', 'Documentary', 'Drama', 'Fantasy', 'Film-Noir', 'Horror', 'Musical',
            'Mystery', 'Romance', 'Sci-Fi', 'Thriller', 'War', 'Western']

header = """user_id,item_id,rating,timestamp,datetime,year,month,day,
            week,dayofweek,age,gender,occupation,zipcode,zipcode1,zipcode2,
            zipcode3,title,release,video_release,imdb,unknown,action,adventure,
            animation,children,comedy,crime,documentary,drama,fantasy,filmnoir,horror,
            musical,mystery,romance,scifi,thriller,war,western,release_date,release_year"""

def parse_dataset(dataset_name = 'ml-100k', example_size = 20000000):

    ########################### PARAMETROS ###########################
    DATA_DIR = 'data'
    DATA_NAME = os.path.join(DATA_DIR, dataset_name)

    MOVIES_DATA_FILE = os.path.join(DATA_NAME, 'movies.csv')
    RATINGS_DATA_FILE = os.path.join(DATA_NAME, 'ratings.csv')

    ITEM_DATA_FILE = os.path.join(DATA_NAME, 'u.item')
    USER_DATA_FILE = os.path.join(DATA_NAME, 'u.user')
    DATA_DATA_FILE = os.path.join(DATA_NAME, 'u.data')

    TRAIN_CSV = os.path.join(DATA_NAME, 'train.csv')
    TEST_CSV = os.path.join(DATA_NAME, 'test.csv')

    TRAIN_DATA_FILES = []
    TRAIN_DATA_FILES.append('ua.base')
    TRAIN_DATA_FILES.append('ub.base')

    TEST_DATA_FILES = []
    TEST_DATA_FILES.append('ua.test')
    TEST_DATA_FILES.append('ub.test')

    TRAIN_DATA_CSV = []
    TRAIN_DATA_CSV.append('train1.csv')
    TRAIN_DATA_CSV.append('train2.csv')
    TRAIN_DATA_CSV.append('train3.csv')
    TRAIN_DATA_CSV.append('train4.csv')
    TRAIN_DATA_CSV.append('train5.csv')

    TEST_DATA_CSV = []
    TEST_DATA_CSV.append('test1.csv')
    TEST_DATA_CSV.append('test2.csv')
    TEST_DATA_CSV.append('test3.csv')
    TEST_DATA_CSV.append('test4.csv')
    TEST_DATA_CSV.append('test5.csv')

    SCRIPT_TRAIN_TEST = os.path.join(DATA_NAME, 'mku.sh')
    SCRIPT_PARTITIONS = os.path.join(DATA_NAME, 'generate.sh')


    user_ids = []
    ## Buscar u.data --> ratings.csv
    print("Checking u.data")
    if os.path.isfile(DATA_DATA_FILE) == False:
        print('--> u.data: ERROR')

        if os.path.isfile(RATINGS_DATA_FILE):
            print('--> ratings.csv: OK')
            print('--> Generating u.data...')
            data_file = open(DATA_DATA_FILE, "w", encoding = "ISO-8859-1")
            with open(RATINGS_DATA_FILE) as ratings_file:
                ratings = ratings_file.read().splitlines()
                for r in ratings[1:]:   #Skip header
                    data_file.write(r.replace(',', '\t'))
                    data_file.write('\n')

                    user_ids.append(r.split(',')[0])
            print('--> u.data: CREATED')
        else:
            print('--> ratings.csv: ERROR')
            print('--> ratings.csv or u.data must exist. Try again with this files')
            sys.exit()


    else:
        print('--> u.data: OK')

    ## Buscar u.user --> ratings.csv
    print("Checking u.user")
    if os.path.isfile(USER_DATA_FILE) == False:
        print('--> u.user: ERROR')
        if os.path.isfile(RATINGS_DATA_FILE):
            print('--> ratings.csv: OK')
            print('--> Generating u.user...')

            #If we have created u.data we get the users id in order to avoid reading all the file again
            if user_ids == []:
                with open(RATINGS_DATA_FILE) as ratings_file:
                    ratings = ratings_file.read().splitlines()
                    for r in ratings[1:]:
                        user_ids.append(r.split(',')[0])

            user_ids_set = sorted(set(user_ids), key=int)
            data_file = open(USER_DATA_FILE, "w", encoding = "ISO-8859-1")

            for u_id in user_ids_set:
                data_file.write(u_id+"|0|gender|occupation|00000")
                data_file.write('\n')
        else:
            print('--> ratings.csv: ERROR')
            print('--> ratings.csv must exist to generate u.user. Try again with this files')
            sys.exit()
    else:
        print('--> u.user: OK')

    ## Buscar u.item --> movies.csv
    print("Checking u.item")
    if os.path.isfile(ITEM_DATA_FILE) == False:
        print('--> u.item: ERROR')
        if os.path.isfile(MOVIES_DATA_FILE):
            print('--> movies.csv: OK')
            print('--> Generating u.item...')

            data_file = open(ITEM_DATA_FILE, "w")
            with open(MOVIES_DATA_FILE) as movies_file:
                movies = movies_file.read().splitlines()

                for r in movies[1:]:   #Skip header

                    #Casos con titulo entre ""
                    r = r.split('"')
                    d = []
                    if (len(r) == 1):   #Caso normal, sin ""
                        d = r[0].split(',')
                    else:
                        d.append(r[0].replace(",", ""))  #Separamos índice
                        d.append(r[1])                  #Guardamos el titulo
                        a = r[2].split(',')             #Separamos lo generos
                        d = d + a
                        #del r[2]

                    movie_genres = []
                    try:
                        movie_genres = d[2].split('|')
                    except:
                        print(d)
                        print(movie_genres)
                        sys.exit()

                    data_file.write(d[0]+'|'+d[1]+'|||IMDb URL')

                    for g in genres:
                        if g in movie_genres:
                            data_file.write('|1')
                        else:
                            data_file.write('|0')

                    data_file.write('\n')
        else:
            print('--> movies.csv: ERROR')
            print('--> movies.csv must exist to generate u.item. Try again with this files')
            sys.exit()
    else:
        print('--> u.item: OK')

    ## Buscar ratings.csv --> u.data
    print("Checking ratings.csv")
    if os.path.isfile(RATINGS_DATA_FILE) == False:
        print('--> ratings.csv: ERROR')
        print('--> Generating ratings.csv...')

        ratings_file = open(RATINGS_DATA_FILE, "w")
        ratings_file.write("userId,movieId,rating,timestamp\n")

        with open(DATA_DATA_FILE, encoding = "ISO-8859-1") as data_file:
            data = data_file.read().splitlines()
            for d in data:
                ratings_file.write(d.replace('\t', ','))
                ratings_file.write('\n')

        print('--> ratings.csv: CREATED')

    else:
        print('--> ratings.csv: OK')


    print("Checking movies.csv")
    if os.path.isfile(MOVIES_DATA_FILE) == False:
        print('--> movies.csv: ERROR')
        print('--> Generating movies.csv...')

        movies_file = open(MOVIES_DATA_FILE, "w")
        movies_file.write("movieId,title,genres\n")
        movie_type = ["unknown", "Action", "Adventure", "Animation", "Children's", "Comedy", "Crime",
                     "Documentary", "Drama", "Fantasy", "Film-Noir", "Horror", "Musical", "Mystery",
                     "Romance", "Sci-Fi", "Thriller", "War", "Western"]

        ## Buscar movies.csv --> u.item
        with open(ITEM_DATA_FILE, encoding = "ISO-8859-1") as item_file:
            items = item_file.read().splitlines()

            for item in items:
                item_splitted = item.split("|")

                if ',' not in item_splitted[1]:
                    movies_file.write(item_splitted[0]+','+item_splitted[1]+',')
                else:
                    movies_file.write(item_splitted[0]+',"'+item_splitted[1]+'",')

                first=0
                for m in range(0, len(movie_type)):
                    if item_splitted[5+m] == '1':
                        if first == 1:
                            movies_file.write('|')
                        movies_file.write(movie_type[m])
                        first = 1

                movies_file.write('\n')
        print('--> movies.csv: CREATED')

    else:
        print('--> movies.csv: OK')


    ## Generate train and test files
    print("Checking uX.base & uX.test")
    ## Check if all train and test files exist
    exist = True
    list_nets = [dI for dI in os.listdir('data/'+dataset_name)]

    for train_file in TRAIN_DATA_FILES:
        if train_file not in list_nets:
            exist = False

    for test_file in TEST_DATA_FILES:
        if test_file not in list_nets:
            exist = False

    ## If any file does not exist, create it
    if (exist == False):
        print('--> Generating uX.base & uX.test...')
        if (os.path.isfile(SCRIPT_TRAIN_TEST) == False):
            print('--> ERROR mku.sh does not exist...')
            print('--> Copy mku.sh, albut.pl & generate.sh from data/generators')
            sys.exit()


        subprocess.call(["./"+SCRIPT_TRAIN_TEST, DATA_NAME, str(int(example_size/5)), str(int(example_size))])

        print('--> uX.base & uX.test: CREATED')

    else:
        print('--> uX.base & uX.test: OK')

    print("Checking train & test files")
    if (os.path.isfile(TRAIN_CSV) == False) or (os.path.isfile(TEST_CSV) == False):

        #Generate one train,csv & test.csv with all the information
        data_dir = 'data/'+dataset_name

        if (dataset_name == 'ml-100k'):
            data = generator100.load_data(data_dir)
            dfs = generator100.process_data(data)
            generator100.save_data(dfs, data_dir)
        else:
            data = generator20.load_data(data_dir)
            generator20.csv_to_db(data)

            # Create db
            # Merge data
        print('--> train & test: CREATED')

    else:
        print('--> train & test: OK')

    #Create partitions of train & test
    print("Checking partitions")
    exist = True

    for train_file in TRAIN_DATA_CSV:
        if train_file not in list_nets:
            exist = False

    for test_file in TEST_DATA_CSV:
        if test_file not in list_nets:
            exist = False

    ## If any file does not exist, create it
    if (exist == False):
        if (os.path.isfile(SCRIPT_PARTITIONS) == False):
            print('--> ERROR generate.sh does not exist...')
            print('--> Copy mku.sh, albut.pl & generate.sh from data/generators')
            sys.exit()

        else:
            f = open(TRAIN_CSV, 'rb')
            n_examples = 0
            buf_size = 1024 * 1024
            read_f = f.raw.read
            buf = read_f(buf_size)
            while buf:
                n_examples += buf.count(b'\n')
                buf = read_f(buf_size)

            subprocess.call(["./"+SCRIPT_PARTITIONS, DATA_NAME, str(int(n_examples/5)), "train", header])

            f = open(TEST_CSV, 'rb')
            n_examples = 0
            buf_size = 1024 * 1024
            read_f = f.raw.read
            buf = read_f(buf_size)
            while buf:
                n_examples += buf.count(b'\n')
                buf = read_f(buf_size)

            subprocess.call(["./"+SCRIPT_PARTITIONS, DATA_NAME, str(int(n_examples/5)), "test", header])

    print("--> Partitions: OK")

if __name__ == '__main__':
    posible_datasets = {'ml-100k': 100000, 'ml-20m': 20000000}

    ##Comprobamos que dataset quiere usar
    pos = 1
    error = 0
    try:
        if sys.argv[pos] not in posible_datasets.keys():
            error = 1
    except IndexError:
            error = 1
    if error == 1:
        print("Posible datasets: ")
        for d in posible_datasets.keys():
            print("\t" + d)
        sys.exit()

    ##Tratamos el dataset y creamos los archivos que falten

    list_data = [dI for dI in os.listdir('data') if os.path.isdir(os.path.join('data',dI))]
    if sys.argv[pos] not in list_data:
        print("Error: Extract dataset " + sys.argv[pos] + " from data/" + sys.argv[pos] + ".zip")
        print("Note: Remember to copy all files from data/generators to the dataset extracted")
        sys.exit()

    parse_dataset(dataset_name = 'ml-100k', example_size = 20000000)
