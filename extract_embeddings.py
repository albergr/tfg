
import os
import sys
import time

import numpy as np
import tensorflow as tf
from annoy import AnnoyIndex
import pandas as pd
from trainers import deep as net_deep
from trainers import deep_fm as net_deep_fm
from trainers import linear_deep as net_linear_deep

from tensorflow.python.framework import ops
from tensorflow.python.client import session
from tensorflow.python.saved_model import loader

import parser_movies as data_parser

DATA_DIR='data'
CHECKPOINTS_LOCATION = 'checkpoints'

def find_embedding_tensor(model_dir, iteration):
    with tf.Session() as sess:
        saver = tf.train.import_meta_graph(os.path.join(model_dir, iteration+'.meta'))
        saver.restore(sess, os.path.join(model_dir, iteration))
        graph = tf.get_default_graph()
        trainable_tensors = map(str, graph.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES))
        tensors = []
        for tensor in set(trainable_tensors):
            if "input_layer/input_layer/" in tensor:
                tensors.append(tensor)

        for t in tensors:
            print(t)    #Todos los emebeddings, user_id_embedding importante; ¿item_id_embedding?

def extract_embeddings(model_dir, iteration, key_embedding):
    with tf.Session() as sess:
        saver = tf.train.import_meta_graph(os.path.join(model_dir, iteration+'.meta'))
        saver.restore(sess, os.path.join(model_dir, iteration))
        graph = tf.get_default_graph()
        weights_tensor = graph.get_tensor_by_name(key_embedding)
        weights = np.array(sess.run(weights_tensor))

    embeddings = {}
    for i in range(weights.shape[0]):
        embeddings[i] = weights[i]

    return embeddings

def build_embeddings_index(num_trees, embeddings, embedding_size, metric = 'angular'):
    total_items = 0
    annoy_index = AnnoyIndex(embedding_size, metric=metric)
    for item_id in embeddings.keys():
        annoy_index.add_item(item_id, embeddings[item_id])
        total_items += 1
    print("{} elements where added to the index".format(total_items))
    annoy_index.build(n_trees=num_trees)
    print("Index is built")
    return annoy_index

def get_similar_movies(movie_id, index, movies_data, num_matches=5):
    similar_movie_ids = index.get_nns_by_item(
        movie_id, num_matches, search_k=-1, include_distances=False)
    similar_movies = movies_data[movies_data['movieId'].isin(similar_movie_ids)].title
    return similar_movies

def recommend_new_movies(userId, embeddings_list, index, movies_data, ratings_data, num_recommendations=5):
    watched_movie_ids = list(ratings_data[ratings_data['userId']==userId]['movieId'])

    user_emebding = embeddings_list[userId]
    similar_movie_ids = index.get_nns_by_vector(
        user_emebding, num_recommendations + len(watched_movie_ids), search_k=-1, include_distances=False)

    recommended_movie_ids = set(similar_movie_ids) - set(watched_movie_ids)
    similar_movies = movies_data[movies_data['movieId'].isin(recommended_movie_ids)].title
    return similar_movies

def get_index(checkpoint_location, name_model, dataset_name, metric = 'angular'):

    model_dir = os.path.join(checkpoint_location, name_model)
    folders = []
    for r, d, f in os.walk(os.path.join(model_dir, "export/exporter")):
        for folder in d:
            if 'variables' not in folder:
                folders.append(os.path.join(model_dir, "export/exporter/"+folder))

    export_dir = max(folders, key=os.path.getmtime)

    loaded_graph = tf.Graph()
    #embeddings_list = []
    key_embedding = ''
    key_embedding_item = ''

    with tf.Session(graph=loaded_graph) as sess:
        tf.saved_model.loader.load(sess, [tf.saved_model.tag_constants.SERVING], export_dir)

        trainable_coll = sess.graph.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)
        vars = {v.name:sess.run(v.value()) for v in trainable_coll}

        for key in vars.keys():

            if "user_id_embedding" in key:
                key_embedding = key

            if "item_id_embedding" in key:
                key_embedding_item = key

        #embeddings_list = vars[key_embedding]



    checkpoints = open ( model_dir+'/checkpoint',"r" )
    lineList = checkpoints.readlines()
    checkpoints.close()
    iteration = lineList[-1][29:-2] #Eliminamos: all_model_checkpoint_paths y las ""; para quedarnos con el nombre del fichero

    ratings_data = pd.read_csv(os.path.join(DATA_DIR, dataset_name+'/ratings.csv'))
    movies_data = pd.read_csv(os.path.join(DATA_DIR, dataset_name+'/movies.csv'))


    embeddings = extract_embeddings(model_dir, iteration, key_embedding_item)
    embeddings_user = extract_embeddings(model_dir, iteration, key_embedding)

    index = build_embeddings_index(num_trees=100, embeddings=embeddings, embedding_size=20, metric=metric)
    user_index = build_embeddings_index(num_trees=100, embeddings=embeddings_user, embedding_size=20, metric=metric)

    directory = os.path.join('export/automatic', name_model)
    if not os.path.exists(directory):
        os.makedirs(directory)

    n_similares = 101   #100 similarities and one more because it takes the item or user itself as distance 0.0

    print("Exporting users...")
    with open(os.path.join(directory, 'users.dat'),"w") as f:
        for users_id in ratings_data.userId.sort_values(ascending=True).unique():
            nn, sim = user_index.get_nns_by_item(users_id-1, n_similares, search_k=-1, include_distances=True )

            for neighbour, similarity in zip(nn, sim):
                if users_id-1 != neighbour:
                    f.write(str(users_id) + "\t" + str(neighbour-1) + "\t" + str(1.0/(similarity+1.0)) + "\n")

    print("Exporting items...")
    with open(os.path.join(directory, 'items.dat'),"w") as f:
        for items_id in ratings_data.movieId.sort_values(ascending=True).unique():   #Get all the ids
            nn, sim = index.get_nns_by_item(items_id-1, n_similares, search_k=-1, include_distances=True )

            for neighbour, similarity in zip(nn, sim):
                if items_id-1 != neighbour:
                    f.write(str(items_id) + "\t" + str(neighbour+1) + "\t" + str(1.0/(similarity+1.0)) + "\n")

    index.unload()

if __name__ == '__main__':

    if "-h" in sys.argv or "--help" in sys.argv:
        print("usage: extract_embeddings.py \t--model | --model NET_NAME [-h | --help] [--frequent_movies]")
        print("\t\t\t\t[--recommend_to_user | --recommend_to_user USER_ID]")
        print("\t\t\t\t[--get_similar_movies | --get_similar_movies MOVIE_ID]")
        print("\t\t\t\t[--exporter FOLDER_NAME] [--dataset NAME_DATASET]")
        print("\t\t\t\t[--metric METRIC] [--save_similarities EXPORT_FOLDER]")
        print("\t\t\t\t[--prepare_data_run_nets DATASET_NAME]")
        print("\t\t\t\t[--getIndexDeep DATASET_NAME] [--getIndexDeep_FM DATASET_NAME]")
        print("\t\t\t\t[--getIndexLinearDeep DATASET_NAME]")



        print("\nobligatory arguments, at least one of them must be introduced:")
        print("--> normal use:")
        print("\t--model \t\t\t\tshow the list of available nets")
        print("\t--model NET_NAME \t\t\textract embeddings from a net, it can be complemented with the optional arguments")
        print("--> extract data:")
        print("\t--prepare_data_run_nets DATASET_NAME \tprepares for the dataset and run all the nets.")
        print("\t\t\t\t\t\tIt is the first step to extract the emebeddings of the nets with the partitions")
        print("\t--getIndexDeep DATASET_NAME \t\textracts 100 NN of the net Deep.")
        print("\t\t\t\t\t\tIt is the second step to extract the emebeddings of the nets with the partitions")
        print("\t--getIndexDeep_FM DATASET_NAME \t\textracts 100 NN of the net Deep_fm.")
        print("\t\t\t\t\t\tIt is the third step to extract the emebeddings of the nets with the partitions")
        print("\t--getIndexLinearDeep DATASET_NAME \textracts 100 NN of the net LinearDeep.")
        print("\t\t\t\t\t\tIt is the fourth step to extract the emebeddings of the nets with the partitions")

        print("\noptional arguments, for normal use:")
        print("\t-h, --help \t\t\t\tshow this help message and exit")
        print("\t--frequent_movies \t\t\tshow the top 15 frequent movies")
        print("\t--recommend_to_user \t\t\tshow recomendations for the five users with more ratings")
        print("\t--recommend_to_user USER_ID \t\tshow recomendations for the user provided")
        print("\t--get_similar_movies \t\t\tshow similar movies for the top 15 frequent movies")
        print("\t--get_similar_movies MOVIE_ID \t\tshow similar movies for the movie provided")
        print("\t--exporter FOLDER_NAME \t\t\trestores the model saved in the folder, it must be inside")
        print("\t\t\t\t\t\tcheckpoints/NET_NAME/export/exporter/FOLDER_NAME. It's generated ")
        print("\t\t\t\t\t\tautomatically when the net is runned. (default: Last run)")
        print("\t--dataset NAME_DATASET \t\t\tname of the dataset which was used when the net was runned. (default: ml-100k)")
        print("\t--metric METRIC \t\t\tmetric used to calculate similarity, it can be: angular,")
        print("\t\t\t\t\t\teuclidean, manhattan, hamming or dot (default: angular)")
        print("\t--save_similarities EXPORT_FOLDER \tsaves for each user his 100 nearest neighbours")
        print("\t\t\t\t\t\tand the same for each item. It also stores their")
        print("\t\t\t\t\t\tsimilarity. It is saved in export/EXPORT_FOLDER")


        sys.exit()

    if ("--prepare_data_run_nets" in sys.argv):
        posible_datasets = {'ml-100k': 100000, 'ml-20m': 20000000}

        ##Comprobamos que dataset quiere usar
        pos = [i for i, s in enumerate(sys.argv) if 'prepare_data_run_nets' in s]
        pos = pos[0]+1
        error = 0
        try:
            if sys.argv[pos] not in posible_datasets.keys():
                error = 1
        except IndexError:
                error = 1
        if error == 1:
            print("Posible datasets: ")
            for d in posible_datasets.keys():
                print("\t" + d)
            sys.exit()

        ##Tratamos el dataset y creamos los archivos que falten

        list_data = [dI for dI in os.listdir('data') if os.path.isdir(os.path.join('data',dI))]
        if sys.argv[pos] not in list_data:
            print("Error: Extract dataset " + sys.argv[pos] + " from data/" + sys.argv[pos] + ".zip")
            print("Note: Remember to copy all files from data/generators to the dataset extracted")
            sys.exit()

        data_parser.parse_dataset(dataset_name=sys.argv[pos], example_size=posible_datasets[sys.argv[pos]])

        print("All datasets CORRECT")

        ## Corremos las redes tantas veces como archivos de train y test generamos = 5 veces

        print("\nRunning nets")
        run_steps = 20000

        for i in range(1,6):
            train_csv = "data/"+sys.argv[pos]+"/train"+str(i)+".csv"
            test_csv = "data/"+sys.argv[pos]+"/test"+str(i)+".csv"

            print("\n\n--> Running deep with:  " + train_csv + " & " + test_csv)
            net_deep.run(train_csv, test_csv, number = i, train_steps=run_steps)


        for i in range(1,6):
            train_csv = "data/"+sys.argv[pos]+"/train"+str(i)+".csv"
            test_csv = "data/"+sys.argv[pos]+"/test"+str(i)+".csv"

            print("\n\n--> Running deep_fm with:  " + train_csv + " & " + test_csv)
            net_deep_fm.run(train_csv, test_csv, number = i, train_steps=run_steps)

        for i in range(1,6):
            train_csv = "data/"+sys.argv[pos]+"/train"+str(i)+".csv"
            test_csv = "data/"+sys.argv[pos]+"/test"+str(i)+".csv"

            print("\n\n--> Running linear_deep with:  " + train_csv + " & " + test_csv)
            net_linear_deep.run(train_csv, test_csv, number = i, train_steps=run_steps)


        print("\nAll checkpoints saved in data/checkpoints/automatic/")
        print("Next step: python extract_embeddings.py --getIndexDeep SAME_DATASET_NAME")

        sys.exit()

    if ("--getIndexDeep_FM" in sys.argv):
        posible_datasets = {'ml-100k': 100000, 'ml-20m': 20000000}

        ##Comprobamos que dataset quiere usar
        pos = [i for i, s in enumerate(sys.argv) if 'getIndexDeep_FM' in s]
        pos = pos[0]+1
        error = 0
        try:
            if sys.argv[pos] not in posible_datasets.keys():
                error = 1
        except IndexError:
                error = 1
        if error == 1:
            print("Posible datasets: ")
            for d in posible_datasets.keys():
                print("\t" + d)
            sys.exit()
        ## Create index and extract embeddings
        for i in range(1,6):
            print("\n--> Exporting Nearest Neighbours of " + "deep_fm"+str(i))
            get_index("checkpoints/automatic", "deep_fm"+str(i), sys.argv[pos], metric = 'angular')

        print("\n\nNext step: python extract_embeddings.py --getIndexLinearDeep SAME_DATASET_NAME")
        sys.exit()

    if ("--getIndexDeep" in sys.argv):
        posible_datasets = {'ml-100k': 100000, 'ml-20m': 20000000}

        ##Comprobamos que dataset quiere usar
        pos = [i for i, s in enumerate(sys.argv) if 'getIndexDeep' in s]
        pos = pos[0]+1
        error = 0
        try:
            if sys.argv[pos] not in posible_datasets.keys():
                error = 1
        except IndexError:
                error = 1
        if error == 1:
            print("Posible datasets: ")
            for d in posible_datasets.keys():
                print("\t" + d)
            sys.exit()

        ## Create index and extract embeddings
        for i in range(1,6):
            print("\n--> Exporting Nearest Neighbours of " + "deep"+str(i))
            get_index("checkpoints/automatic", "deep"+str(i), sys.argv[pos], metric = 'angular')

        print("\n\nNext step: python extract_embeddings.py --getIndexDeep_FM SAME_DATASET_NAME")
        sys.exit()



    if ("--getIndexLinearDeep" in sys.argv):
        posible_datasets = {'ml-100k': 100000, 'ml-20m': 20000000}

        ##Comprobamos que dataset quiere usar
        pos = [i for i, s in enumerate(sys.argv) if 'getIndexLinearDeep' in s]
        pos = pos[0]+1
        error = 0
        try:
            if sys.argv[pos] not in posible_datasets.keys():
                error = 1
        except IndexError:
                error = 1
        if error == 1:
            print("Posible datasets: ")
            for d in posible_datasets.keys():
                print("\t" + d)
            sys.exit()
        ## Create index and extract embeddings
        for i in range(1,6):
            print("\n--> Exporting Nearest Neighbours of " + "linear_deep"+str(i))
            get_index("checkpoints/automatic", "linear_deep"+str(i), sys.argv[pos], metric = 'angular')

        print("\n\nAll neighbours extracted")
        sys.exit()

    if ("--model" in sys.argv):
        pos = [i for i, s in enumerate(sys.argv) if 'model' in s]
        pos = pos[0]+1
        error = 0
        list_nets = [dI for dI in os.listdir('checkpoints') if os.path.isdir(os.path.join('checkpoints',dI))]
        if len(sys.argv) > pos:
            if "--" not in sys.argv[pos]:
                if sys.argv[pos] in list_nets: ## Check if the net is correct
                    MODEL_NAME = sys.argv[pos]
                    error = 1

        if error == 0:
            ##Show all posible nets
            print("Posible nets: ")
            for net in list_nets:
                print("\t" + net)
            sys.exit()
    else:
        print("To see all the posible nets introduce:")
        print("\t--model")

        print("\nTo run a net introduce:")
        print("\t--model NET_NAME")

        print("\nTo show help:")
        print("\t--help")
        print("\t-h")
        sys.exit()


    model_dir = os.path.join(CHECKPOINTS_LOCATION, MODEL_NAME)

    # If the exporter path is given by the user we get it
    exporter = ''
    if ("--exporter" in sys.argv):
        pos = [i for i, s in enumerate(sys.argv) if 'exporter' in s]
        pos = pos[0]+1
        error = 0
        if len(sys.argv) > pos:
            if "--" not in sys.argv[pos]:
                error = 1
                exporter = os.path.join(model_dir, "export/exporter/"+sys.argv[pos])

        if error == 0:
            print("Introduce --exporter FOLDER_NAME")

    folders = []
    # If the exporter path is not defined we get the last export
    if exporter == '':
        for r, d, f in os.walk(os.path.join(model_dir, "export/exporter")):
            for folder in d:
                if 'variables' not in folder:
                    folders.append(os.path.join(model_dir, "export/exporter/"+folder))

        exporter = max(folders, key=os.path.getmtime)
        print("The path has not been specified by the user. Using by default...")

    export_dir = exporter           #Cambiar para cargar desde el que acaba guardando la red
    print("Path: " + export_dir)

    ## Read NeuronalNetwork
    loaded_graph = tf.Graph()
    embeddings_list = []
    key_embedding = ''
    key_embedding_item = ''
    with tf.Session(graph=loaded_graph) as sess:
        tf.saved_model.loader.load(sess, [tf.saved_model.tag_constants.SERVING], export_dir)

        trainable_coll = sess.graph.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)
        vars = {v.name:sess.run(v.value()) for v in trainable_coll}


        for key in vars.keys():
            if "user_id_embedding" in key:
                key_embedding = key
        for key in vars.keys():
            if "item_id_embedding" in key:
                key_embedding_item = key

        embeddings_list = vars[key_embedding]


    checkpoints = open ( model_dir+'/checkpoint',"r" )
    lineList = checkpoints.readlines()
    checkpoints.close()
    iteration = lineList[-1][29:-2] #Eliminamos: all_model_checkpoint_paths y las ""; para quedarnos con el nombre del fichero

    print("Reading for checkpoint: checkpoints/" + MODEL_NAME + '/' + iteration)

    dataset = 'ml-100k'
    if ("--dataset" in sys.argv):
        pos = [i for i, s in enumerate(sys.argv) if 'dataset' in s]
        pos = pos[0]+1
        if len(sys.argv) > pos:
            if "--" not in sys.argv[pos]:
                dataset = sys.argv[pos]

    print("Dataset: " + dataset)

    ## Read data
    try:
        ratings_data = pd.read_csv(os.path.join(DATA_DIR, dataset+'/ratings.csv'))
        movies_data = pd.read_csv(os.path.join(DATA_DIR, dataset+'/movies.csv'))
    except FileNotFoundError:
        print("Error dataset does not exist")
        sys.exit()

    embeddings = extract_embeddings(model_dir, iteration, key_embedding_item)
    embeddings_user = extract_embeddings(model_dir, iteration, key_embedding)

    ## Build index
    metric = 'angular'
    posible_metrics = ['angular', 'euclidean', 'manhattan', 'hamming', 'dot']
    if ("--metric" in sys.argv):
        pos = [i for i, s in enumerate(sys.argv) if 'metric' in s]
        pos = pos[0]+1

        if len(sys.argv) > pos:
            if "--" not in sys.argv[pos]:
                if sys.argv[pos] in posible_metrics:
                    metric = sys.argv[pos]
                else:
                    print("Error, index can not be build. Metric does not exist.")
                    print("Posible metrics:")
                    for m in posible_metrics:
                        print("\t"+m)
                    sys.exit()
    index = build_embeddings_index(num_trees=100, embeddings=embeddings, embedding_size=20, metric=metric)
    user_index = build_embeddings_index(num_trees=100, embeddings=embeddings_user, embedding_size=20, metric=metric)

    ## Export nearest neighbours
    folder = str(time.time()).split('.')[0]
    if ("--save_similarities" in sys.argv):
        pos = [i for i, s in enumerate(sys.argv) if 'save_similarities' in s]
        pos = pos[0]+1
        error = 0

        if len(sys.argv) > pos:
            if "--" not in sys.argv[pos]:
                folder = sys.argv[pos]
                error = 1

        if error == 0:
            print("Path not especified. Using default path  to save similarities")

        directory = os.path.join('export/' + MODEL_NAME, folder)
        print("Export Path: " + directory)

        if not os.path.exists(directory):
            os.makedirs(directory)

        n_similares = 101   #100 similarities and one more because it takes the item or user itself as distance 0.0

        print("Exporting users...")
        with open(os.path.join(directory, 'users.dat'),"w") as f:
            for users_id in ratings_data.userId.sort_values(ascending=True).unique():
                nn, sim = user_index.get_nns_by_item(users_id-1, n_similares, search_k=-1, include_distances=True )

                for neighbour, similarity in zip(nn, sim):
                    if users_id-1 != neighbour:
                        f.write(str(users_id) + "\t" + str(neighbour-1) + "\t" + str(1.0/(similarity+1.0)) + "\n")

        print("Exporting items...")
        with open(os.path.join(directory, 'items.dat'),"w") as f:
            for items_id in ratings_data.movieId.sort_values(ascending=True).unique():   #Get all the ids
                nn, sim = index.get_nns_by_item(items_id-1, n_similares, search_k=-1, include_distances=True )

                for neighbour, similarity in zip(nn, sim):
                    if items_id-1 != neighbour:
                        f.write(str(items_id) + "\t" + str(neighbour+1) + "\t" + str(1.0/(similarity+1.0)) + "\n")


    ## Show frequent movies
    if ("--frequent_movies" in sys.argv):
        frequent_movie_ids = list(ratings_data.movieId.value_counts().index[:15])
        print(movies_data[movies_data['movieId'].isin(frequent_movie_ids)])

    ## Get similar movies
    if ("--get_similar_movies" in sys.argv):
        pos = [i for i, s in enumerate(sys.argv) if 'get_similar_movies' in s]
        pos = pos[0]+1
        recommended = 0

        if len(sys.argv) > pos:
            if "--" not in sys.argv[pos]:
                try:
                    movie_id = int(sys.argv[pos])
                    movie_title = movies_data[movies_data['movieId'] == movie_id].title.values[0]
                    print ("\nMovie: {}".format(movie_title))
                    print (get_similar_movies(movie_id, index, movies_data))
                    recommended = 1
                except ValueError:
                    print("user_id must be an integer")

        if recommended == 0:
            frequent_movie_ids = list(ratings_data.movieId.value_counts().index[:15])

            for movie_id in frequent_movie_ids:
                movie_title = movies_data[movies_data['movieId'] == movie_id].title.values[0]
                print ("\nMovie: {}".format(movie_title))
                print (get_similar_movies(movie_id, index, movies_data))


    ## Recommend to user
    if ("--recommend_to_user" in sys.argv):
        pos = [i for i, s in enumerate(sys.argv) if 'recommend_to_user' in s]
        pos = pos[0]+1
        recommended = 0

        if len(sys.argv) > pos:
            if "--" not in sys.argv[pos]:
                try:
                    user_id = int(sys.argv[pos])
                    print ("\nUser: {}".format(user_id))
                    print (recommend_new_movies(user_id, embeddings_list, index, movies_data, ratings_data))
                    recommended = 1
                except ValueError:
                    print("user_id must be an integer")

        if recommended == 0:
            frequent_user_ids = list((ratings_data.userId.value_counts().index[-350:]))[:5]

            for user_id in frequent_user_ids:
                print ("\nUser: {}".format(user_id))
                print (recommend_new_movies(user_id, embeddings_list, index, movies_data, ratings_data))
