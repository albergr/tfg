package es.uam.eps.tfg.neighbourcomparison;

import es.uam.eps.ir.ranksys.core.Recommendation;
import es.uam.eps.ir.ranksys.core.preference.ConcatPreferenceData;
import es.uam.eps.ir.ranksys.core.preference.PreferenceData;
import es.uam.eps.ir.ranksys.core.preference.SimplePreferenceData;
import es.uam.eps.ir.ranksys.core.util.FastStringSplitter;
import es.uam.eps.ir.ranksys.diversity.sales.metrics.AggregateDiversityMetric;
import es.uam.eps.ir.ranksys.diversity.sales.metrics.GiniIndex;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;
import es.uam.eps.ir.ranksys.fast.index.SimpleFastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.SimpleFastUserIndex;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.fast.preference.SimpleFastPreferenceData;
import es.uam.eps.ir.ranksys.metrics.RecommendationMetric;
import es.uam.eps.ir.ranksys.metrics.SystemMetric;
import es.uam.eps.ir.ranksys.metrics.basic.AverageRecommendationMetric;
import es.uam.eps.ir.ranksys.metrics.basic.NDCG;
import es.uam.eps.ir.ranksys.metrics.basic.Precision;
import es.uam.eps.ir.ranksys.metrics.basic.Recall;
import es.uam.eps.ir.ranksys.metrics.rank.NoDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rank.RankingDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rel.BinaryRelevanceModel;
import es.uam.eps.ir.ranksys.metrics.rel.NoRelevanceModel;
import es.uam.eps.ir.ranksys.nn.item.ItemNeighborhoodRecommender;
import es.uam.eps.ir.ranksys.nn.item.neighborhood.CachedItemNeighborhood;
import es.uam.eps.ir.ranksys.nn.item.neighborhood.ItemNeighborhood;
import es.uam.eps.ir.ranksys.nn.item.neighborhood.TopKItemNeighborhood;
import es.uam.eps.ir.ranksys.nn.item.sim.ItemSimilarity;
import es.uam.eps.ir.ranksys.nn.item.sim.VectorCosineItemSimilarity;
import es.uam.eps.ir.ranksys.nn.neighborhood.Neighborhood;
import es.uam.eps.ir.ranksys.nn.user.UserNeighborhoodRecommender;
import es.uam.eps.ir.ranksys.nn.user.neighborhood.TopKUserNeighborhood;
import es.uam.eps.ir.ranksys.nn.user.neighborhood.UserNeighborhood;
import es.uam.eps.ir.ranksys.nn.user.sim.UserSimilarity;
import es.uam.eps.ir.ranksys.nn.user.sim.VectorCosineUserSimilarity;
import es.uam.eps.ir.ranksys.novelty.longtail.PCItemNovelty;
import es.uam.eps.ir.ranksys.novelty.longtail.metrics.EPC;
import es.uam.eps.ir.ranksys.rec.Recommender;
import es.uam.eps.ir.ranksys.rec.fast.basic.PopularityRecommender;
import es.uam.eps.ir.ranksys.rec.fast.basic.RandomRecommender;
import es.uam.eps.ir.ranksys.rec.runner.RecommenderRunner;
import es.uam.eps.ir.ranksys.rec.runner.fast.FastFilterRecommenderRunner;
import es.uam.eps.ir.ranksys.rec.runner.fast.FastFilters;
import es.uam.eps.ir.ranksys.mf.Factorization;
import es.uam.eps.ir.ranksys.mf.als.HKVFactorizer;
import es.uam.eps.ir.ranksys.mf.rec.MFRecommender;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import static java.lang.Double.parseDouble;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.function.IntPredicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.jooq.lambda.Unchecked;
import org.jooq.lambda.tuple.Tuple;
import org.jooq.lambda.tuple.Tuple3;
import org.ranksys.core.util.tuples.Tuple2id;
import org.ranksys.formats.parsing.Parser;
import static org.ranksys.formats.parsing.Parsers.lp;
import org.ranksys.formats.rec.RecommendationFormat;
import org.ranksys.formats.rec.SimpleRecommendationFormat;
import java.nio.file.Files;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.function.DoubleUnaryOperator;

/**
 *
 */
public class Main {

    public static <U, I> Stream<Tuple3<U, I, Double>> read(String in, Parser<U> up, Parser<I> ip) throws IOException {
        return read(new FileInputStream(in), up, ip);
    }

    public static <U, I> Stream<Tuple3<U, I, Double>> read(InputStream in, Parser<U> up, Parser<I> ip) throws IOException {
        return new BufferedReader(new InputStreamReader(in)).lines().skip(1).map(line -> {
            CharSequence[] tokens = FastStringSplitter.split(line, ',', 4);
            U user = up.parse(tokens[0]);
            I item = ip.parse(tokens[1]);
            double value = parseDouble(tokens[2].toString());

            return Tuple.tuple(user, item, value);
        });
    }

    private static List<String> evaluate(PreferenceData<Long, Long> trainData, PreferenceData<Long, Long> testData, PreferenceData<Long, Long> totalData,
            File f) {
        Double threshold = 4.0;
        // EVALUATED AT CUTOFF 10
        int cutoff = 10;
        // BINARY RELEVANCE
        BinaryRelevanceModel<Long, Long> binRel = new BinaryRelevanceModel<>(false, testData, threshold);
        // NO RELEVANCE
        NoRelevanceModel<Long, Long> norel = new NoRelevanceModel<>();
        // NO RANKING DISCOUNT
        RankingDiscountModel disc = new NoDiscountModel();

        Map<String, SystemMetric<Long, Long>> sysMetrics = new HashMap<>();

        ////////////////////////
        // INDIVIDUAL METRICS //
        ////////////////////////
        Map<String, RecommendationMetric<Long, Long>> recMetrics = new HashMap<>();

        // PRECISION
        recMetrics.put("prec", new Precision<>(cutoff, binRel));
        // RECALL
        recMetrics.put("recall", new Recall<>(cutoff, binRel));
        // nDCG
        recMetrics.put("ndcg", new NDCG<>(cutoff, new NDCG.NDCGRelevanceModel<>(false, testData, threshold)));
        // EPC
        recMetrics.put("epc", new EPC<>(cutoff, new PCItemNovelty<>(trainData), norel, disc));

        // AVERAGE VALUES OF RECOMMENDATION METRICS FOR ITEMS IN TEST
        int numUsers = testData.numUsersWithPreferences();
        recMetrics.forEach((name, metric) -> sysMetrics.put(name, new AverageRecommendationMetric<>(metric, true)));

        ////////////////////
        // SYSTEM METRICS //
        ////////////////////
        sysMetrics.put("aggrdiv", new AggregateDiversityMetric<>(cutoff, norel));
        int numItems = totalData.numItemsWithPreferences();
        sysMetrics.put("gini", new GiniIndex<>(cutoff, numItems));
        sysMetrics.put("usercvg", new UserCoverage());

        RecommendationFormat<Long, Long> format = new SimpleRecommendationFormat<>(lp, lp);
        List<String> lines = new ArrayList<>();
        try {
            format.getReader(f.getAbsolutePath()).readAll().forEach(rec -> sysMetrics.values().forEach(metric -> metric.add(rec)));

            sysMetrics.forEach((name, metric) -> {
                lines.add(f.getName() + "\t" + name + "\t" + metric.evaluate());
            });

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return lines;
    }

    public static void main(String[] args) throws Exception {
        //args = new String[]{"data/ml-20m/train1.csv", "data/ml-20m/test1.csv", "NeighbourComparison/neighbors/users.dat", "NeighbourComparison/neighbors/items.dat"};

        System.out.println("training test user_neighbours item_neighbours netname");
        System.out.println(Arrays.toString(args));

        final String trainDataPath = args[0];
        final String testDataPath = args[1];

        final String userNeighbours = args[2];
        final String itemNeighbours = args[3];

        final String netName = args[4];

        PreferenceData<Long, Long> trainDataPrev = SimplePreferenceData.load(read(trainDataPath, lp, lp));
        PreferenceData<Long, Long> testDataPrev = SimplePreferenceData.load(read(testDataPath, lp, lp));
        PreferenceData<Long, Long> totalData = new ConcatPreferenceData<>(trainDataPrev, testDataPrev);
        FastUserIndex<Long> userIndex = SimpleFastUserIndex.load(totalData.getAllUsers());
        FastItemIndex<Long> itemIndex = SimpleFastItemIndex.load(totalData.getAllItems());
        FastPreferenceData<Long, Long> trainData = SimpleFastPreferenceData.load(read(trainDataPath, lp, lp), userIndex, itemIndex);
        FastPreferenceData<Long, Long> testData = SimpleFastPreferenceData.load(read(testDataPath, lp, lp), userIndex, itemIndex);

        //////////////////
        // RECOMMENDERS //
        //////////////////
        Map<String, Supplier<Recommender<Long, Long>>> recMap = new HashMap<>();

        // random recommendation
        recMap.put("rec_rnd", () -> new RandomRecommender<>(trainData, trainData));

        // most-popular recommendation
        recMap.put("rec_pop", () -> new PopularityRecommender<>(trainData));

        // user-based nearest neighbors
        recMap.put("rec_ub", () -> {
            int k = 100;
            int q = 1;
            double alpha = 0.5;

            UserSimilarity<Long> sim = new VectorCosineUserSimilarity<>(trainData, alpha, true);
            UserNeighborhood<Long> neighborhood = new TopKUserNeighborhood<>(sim, k);

            return new UserNeighborhoodRecommender<>(trainData, neighborhood, q);
        });

        recMap.put("rec_ub_nn", () -> {
            int k = 100;
            int q = 1;
            double alpha = 0.5;

            UserNeighborhood<Long> neighborhood = new FileUserNeighborhood(userIndex, userNeighbours);

            return new UserNeighborhoodRecommender<>(trainData, neighborhood, q);
        });

        // item-based nearest neighbors
        recMap.put("rec_ib", () -> {
            int k = 10;
            int q = 1;
            double alpha = 0.5;

            ItemSimilarity<Long> sim = new VectorCosineItemSimilarity<>(trainData, alpha, true);
            ItemNeighborhood<Long> neighborhood = new TopKItemNeighborhood<>(sim, k);
            neighborhood = new CachedItemNeighborhood<>(neighborhood);

            return new ItemNeighborhoodRecommender<>(trainData, neighborhood, q);
        });

        recMap.put("rec_ib_nn", () -> {
            int k = 10;
            int q = 1;
            double alpha = 0.5;

            ItemNeighborhood<Long> neighborhood = new FileItemNeighborhood(itemIndex, itemNeighbours);
            neighborhood = new CachedItemNeighborhood<>(neighborhood);

            return new ItemNeighborhoodRecommender<>(trainData, neighborhood, q);
        });

        // implicit matrix factorization of Hu et al. 2008
        recMap.put("rec_hkv", () -> {
            int k = 50;
            double lambda = 0.1;
            double alpha = 1.0;
            DoubleUnaryOperator confidence = x -> 1 + alpha * x;
            int numIter = 20;

            Factorization<Long, Long> factorization = new HKVFactorizer<Long, Long>(lambda, confidence, numIter).factorize(k, trainData);

            return new MFRecommender<>(userIndex, itemIndex, factorization);
        });

        ////////////////////////////////
        // GENERATING RECOMMENDATIONS //
        ////////////////////////////////
        Set<Long> targetUsers = testData.getUsersWithPreferences().collect(Collectors.toSet());
        RecommendationFormat<Long, Long> format = new SimpleRecommendationFormat<>(lp, lp);
        Function<Long, IntPredicate> filter = FastFilters.notInTrain(trainData);
        int maxLength = 100;
        RecommenderRunner<Long, Long> runner = new FastFilterRecommenderRunner<>(userIndex, itemIndex, targetUsers.stream(), filter, maxLength);

        recMap.forEach(Unchecked.biConsumer((name, recommender) -> {
            //if (new File(name).exists()) {
            //    System.out.println("Skipping " + name);
            //} else {
            System.out.println("Running " + name);
            try (RecommendationFormat.Writer<Long, Long> writer = format.getWriter(name)) {
                runner.run(recommender.get(), writer);
            }
            //}
        }));

        // evaluation
        File[] filesToEvaluate = new File("./").listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.startsWith("rec_");
            }
        });

        String fPath = "evaluation/" + netName;
        if (!Files.exists(Paths.get("evaluation/"))) {
            Files.createDirectories(Paths.get("evaluation/"));
        }

        List<String> lines = new ArrayList<>();
        Arrays.stream(filesToEvaluate).forEach(f -> lines.addAll(evaluate(trainData, testData, totalData, f)));
        Files.write(Paths.get(fPath), lines, StandardCharsets.UTF_8);
    }

    private static class FileUserNeighborhood extends UserNeighborhood<Long> {

        public FileUserNeighborhood(FastUserIndex<Long> uIndex, String file) {
            super(uIndex, new FileNeighborhood(uIndex, null, file));
        }

    }

    private static class FileItemNeighborhood extends ItemNeighborhood<Long> {

        public FileItemNeighborhood(FastItemIndex<Long> iIndex, String file) {
            super(iIndex, new FileNeighborhood(null, iIndex, file));
        }

    }

    private static class FileNeighborhood implements Neighborhood {

        private Map<Integer, List<Tuple2id>> memoryNeighbors;

        public FileNeighborhood(FastUserIndex<Long> uIndex, FastItemIndex<Long> iIndex, String file) {
            memoryNeighbors = new HashMap<>();

            try (Stream<String> stream = Files.lines(Paths.get(file))) {
                stream.forEach(line -> {
                    String[] toks = line.split("\t");

                    Long e1 = Long.parseLong(toks[0]);
                    Long e2 = Long.parseLong(toks[1]);
                    Double v = Double.parseDouble(toks[2]);

                    int id1 = -1;
                    int id2 = -1;
                    if (uIndex != null) {
                        id1 = uIndex.user2uidx(e1);
                        id2 = uIndex.user2uidx(e2);
                    } else if (iIndex != null) {
                        id1 = iIndex.item2iidx(e1);
                        id2 = iIndex.item2iidx(e2);
                    }

                    if (id1 != -1 && id2 != -1) {
                        List<Tuple2id> neighs = memoryNeighbors.get(id1);
                        if (neighs == null) {
                            neighs = new ArrayList<>();
                            memoryNeighbors.put(id1, neighs);
                        }
                        neighs.add(new Tuple2id(id2, v));
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public Stream<Tuple2id> getNeighbors(int idx) {
            if (memoryNeighbors.containsKey(idx)) {
                return memoryNeighbors.get(idx).stream();
            }
            return Stream.empty();
        }

    }

    public static class UserCoverage implements SystemMetric<Long, Long> {

        private Set<Long> users;

        public UserCoverage() {
            users = new TreeSet<>();
        }

        @Override
        public void add(Recommendation<Long, Long> recommendation) {
            users.add(recommendation.getUser());
        }

        @Override
        public double evaluate() {
            return users.size();
        }

        @Override
        public void combine(SystemMetric<Long, Long> other) {
        }

        @Override
        public void reset() {
            users.clear();
        }

    }
}
