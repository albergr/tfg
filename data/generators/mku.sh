#!/bin/sh

path=$1


trap `rm -f "$path/tmp.$$"; exit 1` 1 2 15

perl "$path/allbut.pl" "$path/ua" 1 10 $3 "$path/u.data"
sort -t"	" -k 1,1n -k 2,2n "$path/ua.base" > "$path/tmp.$$"
mv "$path/tmp.$$" "$path/ua.base"
sort -t"	" -k 1,1n -k 2,2n "$path/ua.test" > "$path/tmp.$$"
mv "$path/tmp.$$" "$path/ua.test"

perl "$path/allbut.pl" "$path/ub" 11 20 $3 "$path/u.data"
sort -t"	" -k 1,1n -k 2,2n "$path/ub.base" > "$path/tmp.$$"
mv "$path/tmp.$$" "$path/ub.base"
sort -t"	" -k 1,1n -k 2,2n "$path/ub.test" > "$path/tmp.$$"
mv "$path/tmp.$$" "$path/ub.test"
