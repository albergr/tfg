#!/bin/sh

path=$1


trap `rm -f "$path/tmp.$$"; exit 1` 1 2 15

for i in 1 2 3 4 5
do
	head -`expr $i \* $2` "$path/$3.csv" | tail -$2 > "$path/tmp.$$"
	if [ $i -ne 1 ];
	then
		echo $4 > "$path/$3$i.csv"
	fi
	sort -t"	" -k 1,1n -k 2,2n "$path/tmp.$$" >> "$path/$3$i.csv"
  rm "$path/tmp.$$"
done
