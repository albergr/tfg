
import os
import sys
import subprocess

from statistics import mean

NAME_DEEP = 'deep'
NAME_DEEP_FM = 'deep_fm'
NAME_LINEAR_DEEP = 'linear_deep'

PATH = 'neighbors/automatic'

class Net:

    def __init__(self, name):
        self.name = name
        self.nets = []

    def add_net(self, net):
        self.nets.append(net)

    def get_nets(self):
        return self.nets

    def get_name(self):
        return self.name

    def __repr__(self):
        return self.name


def cargar_vecinos(type, num_net):

    deep = Net(NAME_DEEP)
    deep_fm = Net(NAME_DEEP_FM)
    linear_deep = Net(NAME_LINEAR_DEEP)

    #Cargamos deep
    with open(os.path.join(PATH, os.path.join(NAME_DEEP + str(num_net), type+'.dat')), "r") as fp:
        line = fp.readline()

        similitudes = {}
        while line:
            line = line.replace('\n', '')
            line = line.split('\t')

            if line[0] in similitudes:  #Si contiene al usuario le añadimos a la lista el siguiente elemento
                similitudes[line[0]].append(line[1])
            else:                       #Si no, añadimos el usuario y la lista
                similitudes[line[0]] = []
                similitudes[line[0]].append(line[1])

            line = fp.readline()

        deep.add_net(similitudes)


    #Cargamos deep_fm
    with open(os.path.join(PATH, os.path.join(NAME_DEEP_FM + str(num_net), type+'.dat')), "r") as fp:
        line = fp.readline()

        similitudes = {}
        while line:
            line = line.replace('\n', '')
            line = line.split('\t')

            if line[0] in similitudes:  #Si contiene al usuario le añadimos a la lista el siguiente elemento
                similitudes[line[0]].append(line[1])
            else:                       #Si no, añadimos el usuario y la lista
                similitudes[line[0]] = []
                similitudes[line[0]].append(line[1])

            line = fp.readline()

        deep_fm.add_net(similitudes)

    #Cargamos linear_deep
    with open(os.path.join(PATH, os.path.join(NAME_LINEAR_DEEP + str(num_net), type+'.dat')), "r") as fp:
        line = fp.readline()

        similitudes = {}
        while line:
            line = line.replace('\n', '')
            line = line.split('\t')

            if line[0] in similitudes:  #Si contiene al usuario le añadimos a la lista el siguiente elemento
                similitudes[line[0]].append(line[1])
            else:                       #Si no, añadimos el usuario y la lista
                similitudes[line[0]] = []
                similitudes[line[0]].append(line[1])

            line = fp.readline()

        linear_deep.add_net(similitudes)

    return [deep, deep_fm, linear_deep]

def calcular_jaccard(vecinos_items, topk, tipo, num_net):

    for v_items in vecinos_items:
        if NAME_DEEP == v_items.get_name():
            deep = v_items

        if NAME_DEEP_FM == v_items.get_name():
            deep_fm = v_items

        if NAME_LINEAR_DEEP == v_items.get_name():
            linear_deep = v_items

    if not os.path.exists("evaluation"):
        os.makedirs("evaluation")
    f = open("evaluation/jaccard_" + str(tipo) + "_top-" + str(topk) + "_dat-" + str(num_net) + ".dat","w+")

    #Deep vs Deep_fm
    iguales = []
    total = 0
    for d, d_fm in zip(deep.get_nets(), deep_fm.get_nets()):
        for element_id in d.keys():
            try:
                net1_top_k = d[element_id][:topk]
                net2_top_k = d_fm[element_id][:topk]
            except:
                net1_top_k = []
                net2_top_k = []
            total = topk
            iguales.append(len(set(net1_top_k) & set(net2_top_k)))

    score = mean(iguales)/(2*total- mean(iguales))
    f.write("Deep vs Deep_fm [k=" + str(topk) + "]: " + str(score) + "\n")
    print("Deep vs Deep_fm [k=" + str(topk) + "]: " + str(score))

    #Deep_fm vs Linear_deep
    iguales = []
    total = 0
    for l_d, d_fm in zip(linear_deep.get_nets(), deep_fm.get_nets()):
        for element_id in l_d.keys():
            try:
                net1_top_k = l_d[element_id][:topk]
                net2_top_k = d_fm[element_id][:topk]
            except:
                net1_top_k = []
                net2_top_k = []
            total = topk
            iguales.append(len(set(net1_top_k) & set(net2_top_k)))

    score = mean(iguales)/(2*total- mean(iguales))
    f.write("Deep_fm vs Linear_deep [k=" + str(topk) + "]: " + str(score) + "\n")
    print("Deep_fm vs Linear_deep [k=" + str(topk) + "]: " + str(score))

    #Deep vs Linear_deep
    iguales = []
    total = 0
    for d, l_d in zip(deep.get_nets(), linear_deep.get_nets()):
        for element_id in d.keys():
            try:
                net1_top_k = d[element_id][:topk]
                net2_top_k = l_d[element_id][:topk]
            except:
                net1_top_k = []
                net2_top_k = []
            total = topk
            iguales.append(len(set(net1_top_k) & set(net2_top_k)))

    score = mean(iguales)/(2*total- mean(iguales))
    f.write("Deep vs Linear_deep [k=" + str(topk) + "]: " + str(score) + "\n")
    print("Deep vs Linear_deep [k=" + str(topk) + "]: " + str(score))

def jaccard():

    for num_net in range(1, 6):
        print("Iteracion = " + str(num_net))
        vecinos_items = cargar_vecinos('items', num_net)

        print("Items")
        topk = 5
        calcular_jaccard(vecinos_items, topk=topk, tipo="items", num_net=num_net)
        print("\n")

        topk = 10
        calcular_jaccard(vecinos_items, topk=topk, tipo="items", num_net=num_net)
        print("\n")

        topk = 15
        calcular_jaccard(vecinos_items, topk=topk, tipo="items", num_net=num_net)
        print("\n")

        topk = 20
        calcular_jaccard(vecinos_items, topk=topk, tipo="items", num_net=num_net)
        print("\n")

        topk = 50
        calcular_jaccard(vecinos_items, topk=topk, tipo="items", num_net=num_net)
        print("\n")

    for num_net in range(1, 6):
        print("Iteracion = " + str(num_net))
        vecinos_users = cargar_vecinos('users', num_net)

        print("Usuarios")
        topk = 5
        calcular_jaccard(vecinos_users, topk=topk, tipo="users", num_net=num_net)
        print("\n")

        topk = 10
        calcular_jaccard(vecinos_users, topk=topk, tipo="users", num_net=num_net)
        print("\n")

        topk = 15
        calcular_jaccard(vecinos_users, topk=topk, tipo="users", num_net=num_net)
        print("\n")

        topk = 20
        calcular_jaccard(vecinos_users, topk=topk, tipo="users", num_net=num_net)
        print("\n")

        topk = 50
        calcular_jaccard(vecinos_users, topk=topk, tipo="users", num_net=num_net)
        print("\n")

if __name__ == '__main__':

    if "--evaluateNeighboursAutomatic" in sys.argv:

        pos = [i for i, s in enumerate(sys.argv) if 'evaluateNeighboursAutomatic' in s]
        pos = pos[0]+1


        path_elements = "neighbors/automatic"
        list_nets = [dI for dI in os.listdir(path_elements) if os.path.isdir(os.path.join(path_elements,dI))]
        print(list_nets)
        print("COMIENZA")
        for net in list_nets:
            nNet = -1
            if "1" in net:
                nNet = 1
            if "2" in net:
                nNet = 2
            if "3" in net:
                nNet = 3
            if "4" in net:
                nNet = 4
            if "5" in net:
                nNet = 5

            if nNet == -1:
                print("ERROR: Can not find training and test file for " + net)
                sys.exit()

            train_file = "data/ml-20m/train" + str(nNet) + ".csv"
            test_file = "data/ml-20m/test" + str(nNet) + ".csv"

            users_file = os.path.join(path_elements, net + "/users.dat")
            items_file = os.path.join(path_elements, net + "/items.dat")

            print("--> Running: " + net)
            subprocess.call(['java', '-jar', 'NeighbourComparison/target/NeighbourComparison-0.4.3.jar', train_file, test_file, users_file, items_file, net])
    elif "--jaccard" in sys.argv:
        jaccard()


    else:
        print("Select an option:")
        print("\t--jaccard")
        print("\t--evaluateNeighboursAutomatic")
