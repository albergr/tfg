
import sqlite3
import csv
import pandas as pd
import dask.dataframe as dd

from pathlib import Path
from pandas import DataFrame

from src.logger import get_logger

logger = get_logger(__name__)

DATA_CONFIG = {
    "users": {"filename": "u.user", "sep": "|", "columns": ["user_id", "age", "gender", "occupation", "zipcode"]},
    "items": {"filename": "u.item", "sep": "|",
              "columns": ["item_id", "title", "release", "video_release", "imdb", "unknown", "action", "adventure",
                          "animation", "children", "comedy", "crime", "documentary", "drama", "fantasy", "filmnoir",
                          "horror", "musical", "mystery", "romance", "scifi", "thriller", "war", "western"]},
    "all": {"filename": "u.data", "sep": "\t", "columns": ["user_id", "item_id", "rating", "timestamp"]},
    "train": {"filename": "ua.base", "sep": "\t", "columns": ["user_id", "item_id", "rating", "timestamp"]},
    "test": {"filename": "ua.test", "sep": "\t", "columns": ["user_id", "item_id", "rating", "timestamp"]},
}

type = {'action': 'int32',
'adventure': 'int32',
'animation': 'int32',
'children': 'int32',
'comedy': 'int32',
'crime': 'int32',
'documentary': 'int32',
'drama': 'int32',
'fantasy': 'int32',
'filmnoir': 'int32',
'horror': 'int32',
'musical': 'int32',
'mystery': 'int32',
'romance': 'int32',
'scifi': 'int32',
'thriller': 'int32',
'unknown': 'int32',
'war': 'int32',
'western': 'int32'}

header = ['user_id','item_id','rating','timestamp','datetime','year','month','day',
            'week','dayofweek','age','gender','occupation','zipcode','zipcode1','zipcode2',
            'zipcode3','title','release','video_release','imdb','unknown','action','adventure',
            'animation','children','comedy','crime','documentary','drama','fantasy','filmnoir','horror',
            'musical','mystery','romance','scifi','thriller','war','western','release_date','release_year']

conn = sqlite3.connect('ml-20m.db')
c = conn.cursor()

def load_data(src_dir="data/ml-20m"):
    print("--> Loading data")
    data = {item: dd.read_csv(str(Path(src_dir, conf["filename"])), sep=conf["sep"],
                              header=None, names=conf["columns"], encoding="latin-1", dtype=type)
            for item, conf in DATA_CONFIG.items()}

    logger.info("data loaded.")
    return data

def csv_to_db(data):

    #Prepare db tables
    users = data["users"]
    users.compute().to_sql('USERS', conn, if_exists='replace', index = False) # Insert the values from the csv file into the table 'USERS'

    items = data["items"]
    items.compute().to_sql('ITEMS', conn, if_exists='replace', index = False) # Insert the values from the csv file into the table 'ITEMS'

    train = data["train"]
    train.compute().to_sql('TRAIN', conn, if_exists='replace', index = False) # Insert the values from the csv file into the table 'TRAIN'

    test = data["test"]
    test.compute().to_sql('TEST', conn, if_exists='replace', index = False) # Insert the values from the csv file into the table 'TEST'

    print('--> Creating train.csv')
    #Create train
    query_clean_train = (
        """
        DROP TABLE IF EXISTS train_table;
        """
        )
    c.execute(query_clean_train)

    query_train = (
        """
        CREATE TABLE train_table AS
    	SELECT
        	user_id, item_id, rating, timestamp,
        	strftime('%Y-%m-%d %H:%M:%S', datetime(timestamp, 'unixepoch')) as datetime,
        	strftime('%Y', datetime(timestamp, 'unixepoch')) as year,
        	strftime('%m', datetime(timestamp, 'unixepoch')) as month,
        	strftime('%d', datetime(timestamp, 'unixepoch')) as day,
        	strftime('%W', datetime(timestamp, 'unixepoch')) as week,
        	strftime('%w', datetime(timestamp, 'unixepoch')) as dayofweek,
        	age, gender, occupation, zipcode,
            SUBSTR(zipcode, 0, 1) AS zipcode1,
            SUBSTR(zipcode, 0, 2) AS zipcode2,
            SUBSTR(zipcode, 0, 3) AS zipcode3,
        	title, release, video_release, imdb,
        	unknown, action, adventure, animation, children, comedy,
        	crime, documentary, drama, fantasy, filmnoir, horror,
        	musical, mystery, romance, scifi, thriller, war, western,
        	release as release_date, strftime('%Y', datetime(release, 'unixepoch')) as release_year
        FROM TRAIN
        JOIN USERS USING (user_id)
        JOIN ITEMS USING (item_id);
        """
    )

    c.execute(query_train)

    with open('data/ml-20m/train.csv', 'w') as write_file:
        csv_out=csv.writer(write_file)
        csv_out.writerow(header)    #Poner cabecera
        for row in c.execute('SELECT * FROM train_table'):

            csv_out.writerow(row)

    print('--> train.csv CREATED')
    print('--> Creating test.csv')
    #Create test
    query_clean_train = (
        """
        DROP TABLE IF EXISTS test_table;
        """
        )
    c.execute(query_clean_train)

    query_train = (
        """
        CREATE TABLE test_table AS
        SELECT
        	user_id, item_id, rating, timestamp,
        	strftime('%Y-%m-%d %H:%M:%S', datetime(timestamp, 'unixepoch')) as datetime,
        	strftime('%Y', datetime(timestamp, 'unixepoch')) as year,
        	strftime('%m', datetime(timestamp, 'unixepoch')) as month,
        	strftime('%d', datetime(timestamp, 'unixepoch')) as day,
        	strftime('%W', datetime(timestamp, 'unixepoch')) as week,
        	strftime('%w', datetime(timestamp, 'unixepoch')) as dayofweek,
        	age, gender, occupation, zipcode,
            SUBSTR(zipcode, 0, 1) AS zipcode1,
            SUBSTR(zipcode, 0, 2) AS zipcode2,
            SUBSTR(zipcode, 0, 3) AS zipcode3,
        	title, release, video_release, imdb,
        	unknown, action, adventure, animation, children, comedy,
        	crime, documentary, drama, fantasy, filmnoir, horror,
        	musical, mystery, romance, scifi, thriller, war, western,
        	release as release_date, strftime('%Y', datetime(release, 'unixepoch')) as release_year
        FROM TEST
        JOIN USERS USING (user_id)
        JOIN ITEMS USING (item_id);
        """
    )

    c.execute(query_train)

    with open('data/ml-20m/test.csv', 'w') as write_file:
        csv_out=csv.writer(write_file)
        csv_out.writerow(header)    #Poner cabecera
        for row in c.execute('SELECT * FROM test_table'):
            csv_out.writerow(row)

    print('--> test.csv CREATED')
